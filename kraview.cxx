/*
 * kraview.cxx A program that extracts mergedimage.png from a Krita document file and passes them to an image viewer for preview.
 * Copyright (C) 2021 半透明狐人間
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>

extern char *__progname;

struct passwd *pw = getpwuid(getuid());

std::string homedir = pw->pw_dir;

std::string *configdata = new std::string[1024];
std::string temppath;
std::string viewerpath;
std::string unzippath = "unzip";
int sevenzip = 0;

std::string msgcfgnotfnd = "Configuration file could not be opend.\n"
													"Please make \".kraviewconfig\" file in your home directory and write the configuration file as below.\n\n"
													"TEMPPATH=/tmp\n"
													"VIEWERCOMMAND=/path/to/imageviewer %s\n\n"
													"TEMPPATH is the absolute path to the temporary file directory.\n"
													"VIEWERCOMMAND is is the path to the image viewer you want to use. %s is automatically populated with the path to the temporary image file this program give to that image viewer.\n\n";



int main(int argc,char**argv) {
	
	std::string configpath = homedir+"/.kraviewconfig";
	
	std::ifstream ifstrm(configpath);
	if (!ifstrm) {
		 std::cout << msgcfgnotfnd;
		exit(1);
    }
	if (argc <=1)  {
		std::cout << "Usage: "+std::string(__progname)+" (.kra file)\n\n"+"The configuration file should be written as below, and placed in your home directory under the name \".kraviewconfig\".\n\n"+"TEMPPATH=/tmp\n"+"VIEWERCOMMAND=/path/to/imageviewer %s\n\n" + "TEMPPATH is the absolute path to the temporary file directory.\nVIEWERCOMMAND is is the path to the image viewer you want to use. %s is automatically populated with the path to the temporary image file this program give to that image viewer.\n";;
		exit(0);
	}
	
	
	
	int rline = 0;
	while (!ifstrm.eof())	{
		if (rline >1024) {
			std::cout << "Configuration file is too big to open!\n";
		exit(2);
		}
		std::getline(ifstrm,configdata[rline]);
		rline++;
	}
	
	int pline = 0;
	while(1) {
		if (pline > rline) {
			std::cout << "Configuration file has an Error : Could not find \"TEMPPATH\".\n";
			exit(3);
		}
		int fres = configdata[pline].find("TEMPPATH");
		if (fres == std::string::npos) {
			pline++;
			continue;
		}  else {
			fres = configdata[pline].find("=")  +1;
			temppath = configdata[pline].substr(fres);
			break;
		}
		pline++;
	}
	
	pline = 0;
	while(1) {
		if (pline > rline) {
			std::cout << "Configuration file has an Error : Could not find \"VIEWERCOMMAND\".\n";
			exit(3);
		}
		int fres = configdata[pline].find("VIEWERCOMMAND");
		if (fres == std::string::npos) {
			pline++;
			continue;
		}  else {
			fres = configdata[pline].find("=")  +1;
			viewerpath = configdata[pline].substr(fres);
			break;
		}
		pline++;
	}


	pline = 0;
	while(1) {
		if (pline > rline) {
			break;
		}
		int fres = configdata[pline].find("USE7Z");
		if (fres == std::string::npos) {
			pline++;
			continue;
		}  else {
			fres = configdata[pline].find("=")  +1;
			if(configdata[pline].substr(fres) == "TRUE") {
				sevenzip = 1;
				unzippath = "7z";
			};
			break;
		}
		pline++;
	}

	pline = 0;
	while(1) {
		if (pline > rline) {
			break;
		}
		int fres = configdata[pline].find("UNZIPPATH");
		if (fres == std::string::npos) {
			pline++;
			continue;
		}  else {
			fres = configdata[pline].find("=")  +1;
			unzippath = configdata[pline].substr(fres);
			break;
		}
		pline++;
	}

	std::string execcmd;	
	if (sevenzip == 0){
		execcmd = unzippath+" "+std::string(argv[1])+" mergedimage.png -d "+temppath;
	} else {
		execcmd = unzippath + " e -o"+temppath+" "+std::string(argv[1])+" mergedimage.png";
	}
	system(execcmd.c_str());
	char *viewcmd = new char[8096];
	//std::cout << execcmd + "\n";
	std::string imgpath = temppath+"/mergedimage.png";
	sprintf(viewcmd,viewerpath.c_str(),imgpath.c_str());
	system(viewcmd);
	//std::cout << std::string(viewcmd) + "\n";
	remove(imgpath.c_str());
	delete[] configdata;
	delete[] viewcmd;
	return 0;
}
