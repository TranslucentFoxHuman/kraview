Kraview

A program that extracts mergedimage.png from a Krita document file and passes them to an image viewer for preview.

Copyright (C) 2021 半透明狐人間

1. What's this?
This is a program to easily preview the Krita document file.
Specifically, it extracts the mergedimage.png from inside the Krita document and passes the file to the specified image viewer.

2. How to use
This program requires "unzip" or "7z" command. Please install UnZip or 7-zip before using this program.
In case of the Debian based GNU/Linux, you can install it with the following command:

sudo apt install unzip

sudo apt install p7zip-full

Next, you have to make a configuration file for this program.
The configuration file should be written as below, and placed in your home directory under the name ".kraviewconfig".

TEMPPATH=/tmp
VIEWERCOMMAND=/path/to/imageviewer %s

TEMPPATH is the absolute path to the temporary file directory.
VIEWERCOMMAND is the path to the image viewer you want to use. %s is automatically populated with the path to the temporary image file this program give to that image viewer.

You can use 7-zip instead of UnZip.
In that case, add the following line to your config file :

USE7Z=TRUE

You can also specify the execution path of UnZip or 7-zip if you want.
This is useful if you have placed UnZip in an unusual location, or if you are running this program on Microsoft Windows. 
This can be omitted if it is not necessary. Also, use the same option if you want to use 7-zip.

UNZIPPATH=/usr/bin/unzip

Now You are ready to use this program!
You can run this program by executing a command like the following:
kraview mypicture.kra

3. How to Build this program
Please install g++ compiler and exec the following command:
g++ kraview.cxx -o kraview

4.License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

4.How to contact to the Author?
You can contact me via direct message on Twitter or Mastodon.
Note : I am a Japanese speaker. I also understand English, but often use automatic translation. In addition, all languages other than Japanese and English are automatically translated.
Please keep in mind that there may be rare mistranslations or misunderstandings.

Twitter : @TlFoxHuman or @Dev_TlFoxHuman1
Mastodon : @translucentfoxhuman@kemonodon.club
